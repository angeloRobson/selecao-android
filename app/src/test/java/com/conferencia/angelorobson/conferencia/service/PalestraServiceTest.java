package com.conferencia.angelorobson.conferencia.service;


import com.conferencia.angelorobson.conferencia.domain.Palestra;

import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.conferencia.angelorobson.conferencia.constantes.Constantes.ALMOCO;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.ARQUIVO;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.CINCO_MINUTOS;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.DOZE_HORAS;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.EVENTO_DE_NETWORKING;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.LIGHTNING;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.NOVE_DA_MANHA;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.TRACK_A;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.TRACK_B;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.TREZE_HORAS;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4.class)
public class PalestraServiceTest {

  private PalestraService palestraService;
  private InputStream inputStream;
  private List<Palestra> palestras;

  @Before
  public void setup() {
    palestraService = new PalestraService();
    palestras = new ArrayList<>();
    inputStream = this.getClass().getClassLoader().getResourceAsStream(ARQUIVO);
    palestras = palestraService.obterPalestras(inputStream);
  }

  @Test
  public void deveRetornar23Registros() {
    assertThat(23, is(palestras.size()));
  }

  @Test
  public void devePossuir12RegistrosNoTrackA(){
    int quantidadeRegistro = 0;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        quantidadeRegistro++;
      }
    }
    assertThat(12, is(quantidadeRegistro));
  }

  @Test
  public void devePossuir11RegistrosNoTrackB(){
    int quantidadeRegistro = 0;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        quantidadeRegistro++;
      }
    }
    assertThat(11, is(quantidadeRegistro));
  }

  @Test
  public void naoDeveRetornarZeroRegistros() throws Exception {
    assertNotEquals(0, is(palestras.size()));
  }

  @Test
  public void naoDevePossuirNumerosNosNomesDasPalestras() {
    for (Palestra palestra : palestras) {
      for (int i = 0; i < palestra.getDescricao().length(); i++) {
        Boolean isContemNumero =  Character.isDigit(palestra.getDescricao().charAt(i));
        assertThat(false, is(isContemNumero));
      }
    }
  }

  @Test
  public void devePossuirIntervaloParaAlmocoNoTrackA(){
    String almoco = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getDescricao().equals(ALMOCO)) {
          almoco = palestra.getDescricao();
        }
      }
    }
    assertThat(ALMOCO, is(almoco));
  }

  @Test
  public void devePossuirIntervaloParaAlmocoNoTrackB(){
    String almoco = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getDescricao().equals(ALMOCO)) {
          almoco = palestra.getDescricao();
        }
      }
    }
    assertThat(ALMOCO, is(almoco));
  }

  @Test
  public void devePossuirEventoNetWorkingNoTrackA(){
    String eventoNetworking = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
          eventoNetworking = palestra.getDescricao();
        }
      }
    }
    assertEquals(EVENTO_DE_NETWORKING, eventoNetworking);
  }

  @Test
  public void devePossuirEventoNetWorkingNoTrackB(){
    String eventoNetworking = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
          eventoNetworking = palestra.getDescricao();
        }
      }
    }
    assertEquals(EVENTO_DE_NETWORKING, eventoNetworking);
  }

  @Test
  public void devePossuirIntervaloDeAlmocoAsDozeHorasDoTrackA(){
    String hora = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getDescricao().equals(ALMOCO)) {
          hora = palestra.getHora();
        }
      }
    }
    assertEquals(DOZE_HORAS, hora);
  }

  @Test
  public void devePossuirIntervaloDeAlmocoAsDozeHorasDoTrackB(){
    String hora = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getDescricao().equals(ALMOCO)) {
          hora = palestra.getHora();
        }
      }
    }
    assertEquals(DOZE_HORAS, hora);
  }

  @Test
  public void devePossuirDoisIntervalosDeAlmocoDuranteTodaAhConferencia(){
    int contador = 0;
    for (Palestra palestra : palestras) {
      if (palestra.getDescricao().equals(ALMOCO)) {
        contador++;
      }
    }
    assertEquals(2, contador);
  }

  @Test
  public void devePossuirDoisEventoDeNetWorkingDuranteTodaAhConferencia(){
    int contador = 0;
    for (Palestra palestra : palestras) {
      if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
        contador++;
      }
    }
    assertEquals(2, contador);
  }

  @Test
  public void devePossuirHorarioNoveDaManhaNaPrimeiraPalestraDoTrackA(){
    String hora = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getHora().equals(NOVE_DA_MANHA)) {
          hora = palestra.getHora();
        }
      }
    }
    assertEquals(NOVE_DA_MANHA, hora);
  }

  @Test
  public void devePossuirHorarioDeNoveDaManhaNaPrimeiraPalestraDoTrackB(){
    String hora = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getHora().equals(NOVE_DA_MANHA)) {
          hora = palestra.getHora();
        }
      }
    }
    assertEquals(NOVE_DA_MANHA, hora);
  }

  @Test
  public void devePossuirHorarioDeTrezeHorasNaPrimeiraPalestraDaTardeDoTrackA(){
    String hora = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getHora().equals(TREZE_HORAS)) {
          hora = palestra.getHora();
        }
      }
    }
    assertEquals(TREZE_HORAS, hora);
  }

  @Test
  public void devePossuirHorarioDeTrezeHorasNaPrimeiraPalestraDaTardeDoTrackB(){
    String hora = null;
    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getHora().equals(TREZE_HORAS)) {
          hora = palestra.getHora();
        }
      }
    }
    assertEquals(TREZE_HORAS, hora);
  }

  @Test(expected = NullPointerException.class)
  public void DeveLancarExcecaoCasoInputStreamEstejaNulo() {
    inputStream = this.getClass().getClassLoader().getResourceAsStream(null);
    palestraService.obterPalestras(inputStream);
  }

  @Test(expected = Exception.class)
  public void DeveLancarExcecaoCasoInputStreamEstejaVazio() {
    inputStream = this.getClass().getClassLoader().getResourceAsStream("");
    palestraService.obterPalestras(inputStream);
  }

  @Test
  public void devePossuirPalestraLigthtingDe5Minutos(){
    String duracao = null;
    for (Palestra palestra : palestras) {
      if (palestra.getDescricao().contains(LIGHTNING)) {
        duracao = palestra.getDuracao();
      }
    }
    assertEquals(CINCO_MINUTOS, duracao);
  }


  @Test
  public void deveIniciarEventoNetWorkingDepoisDasQuatroDaTardeNoTrackA(){
    LocalTime horarioInicial = new LocalTime(16, 0);
    LocalTime horarioEventoNetWorking;
    Integer hora = null;
    Integer minuto = null;

    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
          String [] horario = palestra.getHora().split(":");
           hora = Integer.parseInt(horario[0]);
           minuto = Integer.parseInt(horario[1]);
        }
      }
    }

    horarioEventoNetWorking = new LocalTime(hora, minuto);
    Boolean tempo = horarioEventoNetWorking.isAfter(horarioInicial);

    assertThat(true, is(tempo));
  }

  @Test
  public void deveIniciarEventoNetWorkingDepoisDasQuatroDaTardeNoTrackB(){
    LocalTime horarioInicial = new LocalTime(16, 0);
    LocalTime horarioEventoNetWorking;
    Integer hora = null;
    Integer minuto = null;

    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
          String [] horario = palestra.getHora().split(":");
           hora = Integer.parseInt(horario[0]);
           minuto = Integer.parseInt(horario[1]);
        }
      }
    }

    horarioEventoNetWorking = new LocalTime(hora, minuto);
    Boolean tempo = horarioEventoNetWorking.isAfter(horarioInicial);

    assertThat(true, is(tempo));
  }

  @Test
  public void deveIniciarEventoNetWorkingAntesDasCincoDaTardeNoTrackA(){
    LocalTime horarioLimite = new LocalTime(17, 0);
    LocalTime horarioEventoNetWorking;
    Integer hora = null;
    Integer minuto = null;

    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_A){
        if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
          String [] horario = palestra.getHora().split(":");
           hora = Integer.parseInt(horario[0]);
           minuto = Integer.parseInt(horario[1]);
        }
      }
    }

    horarioEventoNetWorking = new LocalTime(hora, minuto);
    Boolean tempo = horarioEventoNetWorking.isBefore(horarioLimite);

    assertThat(true, is(tempo));
  }

  @Test
  public void deveIniciarEventoNetWorkingAntesDasCincoDaTardeNoTrackB(){
    LocalTime horarioLimite = new LocalTime(17, 0);
    LocalTime horarioEventoNetWorking;
    Integer hora = null;
    Integer minuto = null;

    for (Palestra palestra : palestras) {
      if (palestra.getTrack() == TRACK_B){
        if (palestra.getDescricao().equals(EVENTO_DE_NETWORKING)) {
          String [] horario = palestra.getHora().split(":");
           hora = Integer.parseInt(horario[0]);
           minuto = Integer.parseInt(horario[1]);
        }
      }
    }

    horarioEventoNetWorking = new LocalTime(hora, minuto);
    Boolean tempo = horarioEventoNetWorking.isBefore(horarioLimite);

    assertThat(true, is(tempo));
  }

}
