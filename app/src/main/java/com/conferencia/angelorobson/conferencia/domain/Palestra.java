package com.conferencia.angelorobson.conferencia.domain;


import static com.conferencia.angelorobson.conferencia.constantes.Constantes.ALMOCO;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.CINCO_MINUTOS;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.EVENTO_DE_NETWORKING;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.SESSENTA_MINUTOS;

public class Palestra {

  private String descricao;
  private String duracao;
  private String hora;
  private char track;

  public Palestra() {}

  private Palestra(PalestraBuilder builder)
  {
    this.descricao = builder.descricao;
    this.duracao = builder.duracao;
    this.hora = builder.hora;
    this.track = builder.track;
  }

  public static class PalestraBuilder
  {
    private String descricao;
    private String duracao;
    private String hora;
    private char track;

    public PalestraBuilder () {
    }

    public PalestraBuilder descricao(String descricao)
    {
      this.descricao = descricao;
      return this;
    }

    public PalestraBuilder duracao(String duracao)
    {
      this.duracao = duracao;
      return this;
    }

    public PalestraBuilder hora(String hora)
    {
      this.hora = hora;
      return this;
    }

    public PalestraBuilder track(char track)
    {
      this.track = track;
      return this;
    }

    public PalestraBuilder almoco() {
      this.descricao = ALMOCO;
      this.duracao = SESSENTA_MINUTOS;
      return this;
    }

    public PalestraBuilder lightning() {
      this.duracao = CINCO_MINUTOS;
      return this;
    }

    public PalestraBuilder eventoNetworking() {
      this.descricao = EVENTO_DE_NETWORKING;
      this.duracao = "";
      return this;
    }

    public Palestra build() {
      return new Palestra(this);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      PalestraBuilder that = (PalestraBuilder) o;

      if (track != that.track) return false;
      if (descricao != null ? !descricao.equals(that.descricao) : that.descricao != null)
        return false;
      if (duracao != null ? !duracao.equals(that.duracao) : that.duracao != null) return false;
      return hora != null ? hora.equals(that.hora) : that.hora == null;
    }

    @Override
    public int hashCode() {
      int result = descricao != null ? descricao.hashCode() : 0;
      result = 31 * result + (duracao != null ? duracao.hashCode() : 0);
      result = 31 * result + (hora != null ? hora.hashCode() : 0);
      result = 31 * result + (int) track;
      return result;
    }
  }

  public String getDuracao() {
    return duracao;
  }

  public char getTrack() {
    return track;
  }

  public String getDescricao() {
    return descricao;
  }

  public String getHora() {
    return hora;
  }

  @Override
  public String toString() {
    return
      descricao +
        " \n" + duracao +
        " \n" + hora + "HS";
  }

}
