package com.conferencia.angelorobson.conferencia.holder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.conferencia.angelorobson.conferencia.R;
import com.conferencia.angelorobson.conferencia.domain.Palestra;
import com.jaychang.srv.SimpleCell;
import com.jaychang.srv.SimpleViewHolder;


public class PalestraViewHolder extends SimpleCell<Palestra,PalestraViewHolder.ViewHolder>  {

  public PalestraViewHolder(@NonNull Palestra item) {
    super(item);
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.model;
  }

  @NonNull
  @Override
  protected ViewHolder onCreateViewHolder(ViewGroup parent, View cellView) {
    return new ViewHolder(cellView);
  }

  @Override
  protected void onBindViewHolder(@NonNull ViewHolder viewHolder, int i, @NonNull Context context, Object o) {
    viewHolder.descricaoTxt.setText(getItem().getDescricao());
    viewHolder.horaTxt.setText(getItem().getHora());
    viewHolder.duracaoTxt.setText(getItem().getDuracao());
  }

  static class ViewHolder extends SimpleViewHolder {
    TextView descricaoTxt, horaTxt, duracaoTxt;

    ViewHolder(View itemView) {
      super(itemView);
      descricaoTxt = itemView.findViewById(R.id.descricaoTxt);
      horaTxt = itemView.findViewById(R.id.horaTxt);
      duracaoTxt = itemView.findViewById(R.id.duracaoTxt);
    }
  }
}

