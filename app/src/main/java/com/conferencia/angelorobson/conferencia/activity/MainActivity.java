package com.conferencia.angelorobson.conferencia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.conferencia.angelorobson.conferencia.R;

import net.danlew.android.joda.JodaTimeAndroid;

public class MainActivity extends AppCompatActivity {

  Button btnPalestras;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    JodaTimeAndroid.init(this);

    btnPalestras = findViewById(R.id.btn_palestras);

    btnPalestras.setOnClickListener(view -> {
      Intent intent = new Intent(this, PalestraActivity.class);
      startActivity(intent);
    });

  }

}
