package com.conferencia.angelorobson.conferencia.constantes;

import android.content.Intent;

public final class Constantes {

  public static final String LIGHTNING = "lightning";
  public static final String ALMOCO = "Almoço";
  public static final String SESSENTA_MINUTOS = "60min";
  public static final String CINCO_MINUTOS = "5min";
  public static final String PADRAO_HORA_MINUTO = "HH:mm";
  public static final String EVENTO_DE_NETWORKING = "Evento de Networking";
  public static final Integer CINCO_HORAS_PM_EM_MINUTOS = 480;
  public static final Integer QUATRO_HORAS_PM_EM_MINUTOS = 420;
  public static final Integer DUZENTOS_E_QUARENTA_MINUTOS = 240;
  public static final Integer CENTO_E_OITENTA_MINUTOS = 180;
  public static final Integer ZERO_MINUTO = 0;
  public static final char TRACK_A = 'A';
  public static final char TRACK_B = 'B';
  public static final String TRACK = "Track ";
  public static final String ARQUIVO = "proposals.txt";
  public static final Integer HORA_INICIAL = 9;
  public static final Integer MINUTO_INICIAL = 0;
  public static final Integer QUARENTA_MINUTOS = 40;
  public static final String DOZE_HORAS = "12:00";
  public static final String NOVE_DA_MANHA = "09:00";
  public static final String TREZE_HORAS = "13:00";
}
