package com.conferencia.angelorobson.conferencia.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.conferencia.angelorobson.conferencia.R;
import com.conferencia.angelorobson.conferencia.domain.Palestra;
import com.conferencia.angelorobson.conferencia.holder.PalestraViewHolder;
import com.conferencia.angelorobson.conferencia.service.PalestraService;
import com.jaychang.srv.SimpleRecyclerView;
import com.jaychang.srv.decoration.SectionHeaderProvider;
import com.jaychang.srv.decoration.SimpleSectionHeaderProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.conferencia.angelorobson.conferencia.constantes.Constantes.TRACK;

public class PalestraActivity extends AppCompatActivity {

  SimpleRecyclerView simpleRecyclerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_palestra);

    simpleRecyclerView = findViewById(R.id.recyclerView);
    this.adicionarRecyclerHeaders();
    this.popularRecycleView();
  }

  private void adicionarRecyclerHeaders() {
    SectionHeaderProvider<Palestra> sectionHeaderProvider = new SimpleSectionHeaderProvider<Palestra>() {

      @NonNull
      @Override
      public View getSectionHeaderView(@NonNull Palestra palestra, int i) {
        View view = LayoutInflater.from(PalestraActivity.this).inflate(R.layout.header, null, false);
        TextView textView =  view.findViewById(R.id.headerTxt);
        textView.setText(String.valueOf(TRACK + palestra.getTrack()));
        return view;
      }

      @Override
      public boolean isSameSection(@NonNull Palestra palestra, @NonNull Palestra proximaPalestra) {
        return palestra.getTrack() == proximaPalestra.getTrack();
      }

      @Override
      public boolean isSticky() {
        return true;
      }
    };

    simpleRecyclerView.setSectionHeader(sectionHeaderProvider);
  }

  private void popularRecycleView() {
    List<Palestra> palestras = obterPalestras();
    Collections.sort(palestras, (Palestra, proximaPalestra) -> Palestra.getTrack() - proximaPalestra.getTrack());
    List<PalestraViewHolder> viewHolders = new ArrayList<>();

    for (Palestra palestra : palestras) {
      PalestraViewHolder holder = new PalestraViewHolder(palestra);
      viewHolders.add(holder);
    }
    simpleRecyclerView.addCells(viewHolders);
  }

  private ArrayList<Palestra> obterPalestras() {
    ArrayList<Palestra> palestras = new ArrayList<>();
    PalestraService palestraService = new PalestraService();
    List<Palestra> palestrasRetornada = palestraService.obterPalestras(getResources().openRawResource(R.raw.proposals));
    palestras.addAll(palestrasRetornada);
    return palestras;
  }
}
