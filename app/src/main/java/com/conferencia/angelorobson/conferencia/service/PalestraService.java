package com.conferencia.angelorobson.conferencia.service;

import com.conferencia.angelorobson.conferencia.domain.Palestra;
import com.conferencia.angelorobson.conferencia.domain.Palestra.PalestraBuilder;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static com.conferencia.angelorobson.conferencia.constantes.Constantes.CENTO_E_OITENTA_MINUTOS;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.CINCO_HORAS_PM_EM_MINUTOS;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.DUZENTOS_E_QUARENTA_MINUTOS;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.HORA_INICIAL;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.LIGHTNING;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.MINUTO_INICIAL;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.PADRAO_HORA_MINUTO;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.TRACK_A;
import static com.conferencia.angelorobson.conferencia.constantes.Constantes.ZERO_MINUTO;

public class PalestraService {

  private Integer totalMinutoAtual;
  private char track;
  private List<Palestra> palestras;
  private Palestra palestra;
  private DateTimeFormatter dateTimeFormatter;
  private List<String> linhasArquivo;

  public PalestraService() {
    totalMinutoAtual = ZERO_MINUTO;
    track = TRACK_A;
    palestras = new ArrayList<>();
    palestra = new Palestra();
    dateTimeFormatter = DateTimeFormat.forPattern(PADRAO_HORA_MINUTO);
    linhasArquivo = new LinkedList<>();
  }

  public List<Palestra> obterPalestras(InputStream inputStream) {
    try {
      InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

      BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

      List<String> conteudoArquivoRetornado = lerConteudoArquivo(bufferedReader);

      linhasArquivo.addAll(conteudoArquivoRetornado);

      inputStream.close();

      ordenarLinhasDoArquivoEmOrdemDescendente(linhasArquivo);

      for (String linhaArquivo : linhasArquivo) {

        if (isPeriodoManha(totalMinutoAtual)) {

          if (totalMinutoAtual == 0) {
            popularPalestraInicial(linhaArquivo);

          } else {
            popularPalestra(linhaArquivo);
          }

        }

        if (isHorarioAlmoco(totalMinutoAtual)) {
          popularObjetoAlmoco();
        }

        if (isPeriodoTarde(totalMinutoAtual)) {

          if (isLimitePalestraPeriodoTarde(palestra.getDuracao(), totalMinutoAtual)) {

            popularPalestra(linhaArquivo);

            totalMinutoAtual = incrementartarMinutos(totalMinutoAtual, palestra.getDuracao());

            popularEventoNetWorking(totalMinutoAtual);

            totalMinutoAtual = ZERO_MINUTO;

            track = incrementarTrack(track);
          } else {
            if (isPalestraLighting(linhaArquivo)) {
              popularPalestraLighting(linhaArquivo);
            } else {
              popularPalestra(linhaArquivo);
            }
          }
        }

        if (!"".equals(palestra.getDuracao())) {
          totalMinutoAtual = incrementartarMinutos(totalMinutoAtual, palestra.getDuracao());
        }
      }

      popularEventoNetWorking(totalMinutoAtual);

    } catch (IOException e) {
      e.printStackTrace();
    }
    return palestras;
  }

  private void popularPalestraInicial(String linhaArquivo) {
    palestra = new PalestraBuilder()
      .descricao(obterDescricaoPalestra(linhaArquivo))
      .duracao(obterDuracaoPalestra(linhaArquivo))
      .hora(dateTimeFormatter.print(obterHorarioInicialPalestra()))
      .track(track)
      .build();

    palestras.add(palestra);
  }

  private void popularPalestra(String linhaArquivo) {
    palestra = new PalestraBuilder()
      .descricao(obterDescricaoPalestra(linhaArquivo))
      .duracao(obterDuracaoPalestra(linhaArquivo))
      .hora(obterHorarioPalestra(totalMinutoAtual))
      .track(track)
      .build();
    palestras.add(palestra);
  }

  private void popularPalestraLighting(String linhaArquivo) {
    if (isPalestraLighting(linhaArquivo)) {
      palestra = new PalestraBuilder().lightning()
        .descricao(linhaArquivo)
        .hora(obterHorarioPalestra(totalMinutoAtual))
        .track(track)
        .build();

      palestras.add(palestra);
    }
  }

  private void popularObjetoAlmoco() {
    palestra = new PalestraBuilder().almoco()
      .hora(obterHorarioPalestra(totalMinutoAtual))
      .track(track)
      .build();
    palestras.add(palestra);

    totalMinutoAtual = incrementartarMinutos(totalMinutoAtual, palestra.getDuracao());

  }

  private void popularEventoNetWorking(Integer minuto) {
    palestra = new PalestraBuilder()
      .eventoNetworking()
      .hora(obterHorarioPalestra(minuto))
      .track(track)
      .build();

    palestras.add(palestra);
  }

  private boolean isLimitePalestraPeriodoTarde(String duracao, Integer totalMinutoAtual) {
    totalMinutoAtual += obterApenasNumerosDaDuracaoPalestra(duracao);
    return totalMinutoAtual >= CINCO_HORAS_PM_EM_MINUTOS;
  }

  private List<String> lerConteudoArquivo(BufferedReader bufferedReader) throws IOException {
    List<String> conteudoArquivo = new LinkedList<>();
    String linhaArquivo;
    while ((linhaArquivo = bufferedReader.readLine()) != null) {
      conteudoArquivo.add(linhaArquivo);
    }
    return conteudoArquivo;
  }

  private void ordenarLinhasDoArquivoEmOrdemDescendente(List<String> linhasArquivo) {
    Collections.sort(linhasArquivo, (lhs, rhs) -> (lhs.compareToIgnoreCase(rhs) * (-1)));
  }

  private boolean isPalestraLighting(String linhaArquivo) {
    return linhaArquivo.contains(LIGHTNING);
  }

  private String obterHorarioPalestra(Integer minuto) {
    LocalTime horarioInicial = obterHorarioInicialPalestra();
    return dateTimeFormatter.print(horarioInicial.plusMinutes(minuto));
  }

  private boolean isPeriodoManha(Integer totalMinutoAtual) {
    return totalMinutoAtual >= ZERO_MINUTO && totalMinutoAtual < CENTO_E_OITENTA_MINUTOS;
  }

  private String obterDescricaoPalestra(String linhaArquivo) {
    return linhaArquivo.substring(0, linhaArquivo.length() - 6);
  }

  private String obterDuracaoPalestra(final String linhaDoArquivo) {
    return linhaDoArquivo.substring(linhaDoArquivo.lastIndexOf(" ") + 1);
  }

  private LocalTime obterHorarioInicialPalestra() {
    return new LocalTime(HORA_INICIAL, MINUTO_INICIAL);
  }

  private boolean isHorarioAlmoco(Integer totalMinutoAtual) {
    return totalMinutoAtual >= CENTO_E_OITENTA_MINUTOS && totalMinutoAtual < DUZENTOS_E_QUARENTA_MINUTOS;
  }

  private Integer incrementartarMinutos(Integer quantidadeTotalMinutoAtual, String duracao) {
    quantidadeTotalMinutoAtual += obterApenasNumerosDaDuracaoPalestra(duracao);
    return quantidadeTotalMinutoAtual;
  }

  private boolean isPeriodoTarde(Integer totalMinutoAtual) {
    return totalMinutoAtual >= DUZENTOS_E_QUARENTA_MINUTOS && totalMinutoAtual < CINCO_HORAS_PM_EM_MINUTOS;
  }

  private char incrementarTrack(char track) {
    track++;
    return track;
  }

  private Integer obterApenasNumerosDaDuracaoPalestra(String duracao) {
    return Integer.parseInt(duracao.replaceAll("[^0-9]", ""));
  }

}
